const User = require("../model/user")
module.exports =async (req, res, next)=>{
    let id = (req.headers.id)
    try{
        const user =await User.findOne({
            _id: id
        })
        
        if(!user.isLogin){
            res.status(400).send("User not logged in")
        }
        else{
            next()
        }

    }
    catch (e){
        res.status(400).send({error:e.message,message:"invalid user"})
    }
}