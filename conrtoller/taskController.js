const Task = require('../model/task')

class TaskController {
    async addTask (task,userId){
        return Task.create({task,userId})
    }

    async over (_id,userId){
        return Task.update({
            _id,userId
        },{
            isOver:true
        })
    }
    
    async task (userId){
        return Task.find({
            userId
        })
    }
}

module.exports = TaskController