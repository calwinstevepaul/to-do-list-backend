const User = require('../model/user')
const bcrypt = require('bcrypt');

class AuthController {
    async signup (name, email, password){
        const hash = await bcrypt.hashSync(password, 10);

        return User.create({
            name: name,
            email:email,
            password:hash
        })
    }

    async login (email, password){
        return User.findOne({
            email: email
        })
    }
    async isLogin(id){
        return User.update({
                _id:id
            },
            {
                isLogin:true
            }
        )
    }
    async logout(id){
        return User.update({
            _id:id
        },{
            isLogin:false
        })
    }
}

module.exports = AuthController