var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var task = new Schema({
    task:String, 
    userId:String,   
    isOver:{type:Boolean,default:false}
  });


var Task = mongoose.model('tasks',task);
module.exports = Task; 