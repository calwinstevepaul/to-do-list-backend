const express = require('express');
const mongoose = require('mongoose')
const app = express();
const cors = require("cors");

mongoose.connect('mongodb://localhost/loginSignup')
.then(()=>console.log("Connected to DB"))
.catch(()=>console.error("Not connect to db"))

const authRout = require('./router/authRout')
const taskRout = require('./router/taskRout')

app.use(express.json());
app.use(cors())
app.use('/auth',authRout) 
app.use('/task',taskRout) 

const port = 9000; 
app.listen(port, () => {
    console.log(`listening on port ${port}......`)
})