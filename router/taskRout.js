const express = require('express');
const router = express.Router()
const middleware = require('../Middleware/middleware')
const Controller = require('../conrtoller/taskController')
const controller = new Controller()

router.get('/',middleware ,(req, res) => {
    const {userId} = req.query

    controller.task(userId)
    .then(data=>{
        res.send(data)
    })
    .catch(e=>{
        res.status(400).send(e.message)
    })
    
})

router.post('/addTask',middleware ,(req, res) => {
    const {task,id} = req.body

    controller.addTask(task,id)
    .then(data=>{
        res.send(data)
    })
    .catch(e=>{
        res.status(400).send(e.message)
    })
    
})

router.put('/over',middleware ,(req, res) => {
    const {taskId,userId} = req.body

    controller.over(taskId,userId)
    .then(data=>{
        res.send(data)
    })
    .catch(e=>{
        res.status(400).send(e.message)
    })
    
})





module.exports = router; 