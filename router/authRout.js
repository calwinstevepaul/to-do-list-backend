const express = require('express');
const router = express.Router()
const bcrypt = require('bcrypt');

const Controller = require('../conrtoller/authController')
const controller = new Controller()

router.post('/signup', (req, res) => {
    const {name, email, password} = req.body
    controller.signup(name, email, password)
        .then(data=>{
            res.send(data)
        })
        .catch(e=>{
            res.status(400).send(e.message)
        })
})

router.post('/login', (req, res)=>{
    const {email, password} = req.body
    controller.login(email)
    .then(data=>{
       if(data){
            bcrypt.compare(password, data.password, function(err, result) {
                if(result){
                    controller.isLogin(data._id)
                    res.send({
                        status:"successful",
                        login:true,
                        userId:data._id
                    })
                }
                else{
                    res.status(400).send({
                        status:"Wrong Password",
                        login:false,
                        userId:data._id
                    })
                }
            });
       }
       else{
        res.status(400).send({
            status:"Invalid Email",
            login:false
        })
       }
    })
    .catch(e=>{
        res.status(400).send(e.message)
    })
})

router.put('/logout', (req, res) => {
    const {id} = req.body
    controller.logout(id)
        .then(data=>{
            res.send(data)
        })
        .catch(e=>{
            res.status(400).send(e.message)
        })
})


module.exports = router; 